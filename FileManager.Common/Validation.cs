﻿using System;

namespace FileManager.Common
{
    public class Validation
    {
        public string PathName { get; set; }
        public string Name { get; set; }
        public string ValidName { get; set; }
        public bool CharValid { get; set; }

        public void GetPath()
        {
            Console.Write("Enter Path Name Of File: ");
            string pathName = Console.ReadLine();

            Console.Write("Enter Name Of File For Search: ");
            string name = Console.ReadLine();

            string valid = $"../../../../{pathName}/{name}{".txt".ToLower()}";

            // Gentler Character Filter
            bool charValid = !valid.Contains("!") && !valid.Contains("@") && !valid.Contains("#") && !valid.Contains("$") && !valid.Contains("[") && !valid.Contains("]")
                             && !valid.Contains("%") && !valid.Contains("^") && !valid.Contains("&") && !valid.Contains("*") && !valid.Contains("=") && !valid.Contains("_")
                             && !valid.Contains("|") && !valid.Contains("~") && !valid.Contains("<") && !valid.Contains(">") && !valid.Contains("?") && !valid.Contains(@"\");

            PathName = pathName;           
            Name = name;
            ValidName = valid;
            CharValid = charValid;
        }    
    }
}
