﻿using FileManager.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FileManager.App
{
    public static class Program
    {
        public static bool repeat = true;

        public static void Main(string[] args)
        {
            MainMenu();
            Console.ReadKey();
        }

        public static void MainMenu()
        {
            User user = new User();
            AddOrDeleteData addOrDelete = new AddOrDeleteData();
            ShowInfo showInfos = new ShowInfo();

            try
            {
                while (repeat)
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("Menu: {0}", user.UserId++);
                    Console.WriteLine("Please select a item:");
                    Console.ResetColor();

                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.WriteLine("1) Import Information\n2) Show Name All Files\n3) Show Info One User\n" +
                        "4) Create a Directory\n5) Delete One File\n6) Delete a Directory\n7) New\n8) Exit");
                    Console.ResetColor();

                    int selector = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                    Console.WriteLine("");
                    switch (selector)
                    {
                        case 1:
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("<Import your information>");
                            Console.ResetColor();
                            addOrDelete.GetInfo();
                            break;
                        case 2:
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("<View Name All Files>");
                            Console.ResetColor();
                            showInfos.ShowNameFiles();
                            break;
                        case 3:
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("<View Info One User>");
                            Console.ResetColor();
                            showInfos.ShowInfoOneUser();
                            break;
                        case 4:
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("<Select Location for Directories>");
                            Console.ResetColor();
                            addOrDelete.CreateDirectory();
                            break;
                        case 5:
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("<Select 1 File>");
                            Console.ResetColor();
                            addOrDelete.DeleteFile();
                            break;
                        case 6:
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("<Select 1 Directory>");
                            Console.ResetColor();
                            addOrDelete.DeleteDirectory();
                            break;
                        case 7:
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("<New>");
                            Console.ResetColor();
                            addOrDelete.New();                            
                            break;
                        case 8:
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("<Exit>");
                            Console.ResetColor();
                            repeat = false;
                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("invalid character");
                            Console.ResetColor();
                            break;
                    }
                }
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please select just of Characters reserved List !");
                Console.ResetColor();
                Console.WriteLine("");
                MainMenu();
            }
        }
    }
}
