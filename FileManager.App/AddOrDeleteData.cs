﻿using FileManager.Common;
using FileManager.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileManager.App
{
    public class AddOrDeleteData : User
    {
        public bool IsRegistered = false;  // Field for input correctness
        public string PathFile { get; set; }
        public List<AddOrDeleteData> AddOrDeletes { get; set; }  // for setter Value by Imported Data

        public AddOrDeleteData()
        {
            AddOrDeletes = new List<AddOrDeleteData>();
        }

        public void GetInfo()
        {
            Validation valid = new Validation();
            try
            {
                while (IsRegistered || valid.CharValid || FirstName != "" || Age != 0 || Age! < 9 || Age! > 120 || CodeMeli != 00 || Mobile != 0 || Email != "" || Address != "" || Salary != 0)
                {
                    Console.Write("Please Enter Directory: ");
                    PathFile = Console.ReadLine();

                    Console.Write("FirstName: ");
                    FirstName = Console.ReadLine();
                    if (FirstName == "" || FirstName == $"{!valid.CharValid}")
                        break;

                    Console.Write("LastName: ");
                    LastName = Console.ReadLine();

                    Console.Write("Age (range 10 - 100): ");
                    Age = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                    if (Age < 10 || Age > 100)
                        break;

                    Console.Write("National code (without init 00): ");
                    CodeMeli = Convert.ToInt64(Console.ReadLine()); ;
                    if (CodeMeli == 00 || CodeMeli == 0)
                        break;

                    Console.Write("Gender (M or m for Male / F or f for Female): ");
                    char gender = char.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                    if (gender == 'M' || gender == 'm')
                        Gender = GenderType.Male;
                    else if (gender == 'F' || gender == 'f')
                        Gender = GenderType.Female;
                    else
                        break;

                    Console.Write("Marital status (S or s for Single / M or m for Married): ");
                    char married = char.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                    if (married == 'S' || married == 's')
                        Marital = MaritalType.Single;
                    else if (married == 'M' || married == 'm')
                        Marital = MaritalType.Married;
                    else
                        break;

                    Console.Write("Mobile (without init 0): ");
                    Mobile = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                    if (Mobile == 0)
                        break;

                    Console.Write("Email: ");
                    Email = Console.ReadLine();
                    if (Email == "")
                        break;

                    Console.Write("Address: ");
                    Address = Console.ReadLine();
                    if (Address == "")
                        break;

                    Console.Write("Salary: ");
                    Salary = Convert.ToInt64(Console.ReadLine());
                    if (Salary == 0)
                        break;

                    Console.Write("Description: ");
                    Description = Console.ReadLine();
                    if (Description == "" || Description != "")
                        break;
                }
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Bad Imported !");
                Console.ResetColor();
            }
            finally
            {
                IsRegistered = true;

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("<your Information>");
                Console.ResetColor();

                string FullName = FirstName + LastName;

                AddOrDeleteData[] add = new AddOrDeleteData[]
                {
                    new AddOrDeleteData(){ FirstName= FullName, Age= Age, CodeMeli= CodeMeli, Gender= Gender, Marital= Marital, Mobile= Mobile, Email= Email, Address= Address, Description= Description}
                };

                var query = from u in add
                            select new { u.FirstName, u.Age, u.CodeMeli, u.Gender, u.Marital, u.Mobile, u.Email, u.Address, u.Description};

                foreach (var q in query)
                {
                    Console.WriteLine("FullName: {0} - Age: {1} - CodeMeli: {2} - Gender: {3} - Marital: {4} - Mobile: {5} - Email: {6} - Address: {7} - Description: {8}", q.FirstName, q.Age, q.CodeMeli, q.Gender, q.Marital, q.Mobile, q.Email, q.Address, q.Description);
                }

                //Console.WriteLine("your Name: {0}", FirstName + " " + LastName);
                //Console.WriteLine("you are {0} years old", Age);
                //Console.WriteLine("your national code: {0}", "00" + CodeMeli);
                //Console.WriteLine("you are {0}", Gender);
                //Console.WriteLine("you are {0}", Marital);
                //Console.WriteLine("your mobile: {0}", "0" + Mobile);
                //Console.WriteLine("your email: {0}", Email);
                //Console.WriteLine("your salary: {0}", Salary);
                //Console.WriteLine("your address: {0}", Address);
                //Console.WriteLine("description: {0}", Description);

                //UserId++; //???


                bool IsContinue = true;
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Continue? (YES) y OR Y , (NO) n OR N");
                Console.ResetColor();
                char select = char.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

                while (IsContinue)
                {
                    if (select == 'y' || select == 'Y')
                    {
                        AddOrDeletes.Add(this);
                        SaveInfo();
                        if (Directory.Exists(PathFile))  //???
                            Console.WriteLine("File Created");
                        Console.WriteLine("");
                        IsContinue = true;
                        GetInfo();
                    }
                    else if (select == 'n' || select == 'N')
                    {
                        AddOrDeletes.Add(this);
                        SaveInfo();
                        if (Directory.Exists(PathFile))  //???
                            Console.WriteLine("File Created");
                        IsContinue = false;
                        Program.MainMenu();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("Invalid character - Just (y OR Y , n OR N)");
                        Console.ResetColor();
                        select = char.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                    }
                }
            }
        }

        // This method stores user information in a File
        public void SaveInfo()
        {
            string path = $"../../../../{PathFile}/{FirstName}{LastName}{".txt".ToLower()}";
            try
            {
                using (StreamWriter file = File.CreateText(path))
                {
                    file.WriteLine("<your Information>");
                    file.WriteLine("your Name: {0}", FirstName + " " + LastName);
                    file.WriteLine("you are {0} years old", Age);
                    file.WriteLine("your national code: {0}", "00" + CodeMeli);
                    file.WriteLine("you are {0}", Gender);
                    file.WriteLine("you are {0}", Marital);
                    file.WriteLine("your mobile: {0}", "0" + Mobile);
                    file.WriteLine("your email: {0}", Email);
                    file.WriteLine("your salary: {0}", Salary);
                    file.WriteLine("your address: {0}", Address);
                    file.WriteLine("description: {0}", Description);
                }
            }
            catch
            {
                Console.WriteLine("Path Not Found !");
            }
        }

        public void CreateDirectory()
        {
            Console.Write("Enter Name Of Directory For Search: ");
            string name = Console.ReadLine();

            string path = "../../../" + name;

            if (path != "../../../")
            {
                Directory.CreateDirectory(path);
                Console.WriteLine("Directory Created !");
            }
            else
                Console.WriteLine("Please Enter name For Directory !");
        }

        public void DeleteFile()
        {
            Console.Write("Enter Name of File for Search: ");
            string name = Console.ReadLine();

            string path = "../../../../" + name + " ".ToLower();
            if (File.Exists(path))
            {
                var file = path;
                File.Delete(file);
                Console.WriteLine("File Deleted !");
            }
            else
                Console.WriteLine("File Not Exists !");
        }

        public void DeleteDirectory()
        {
            Console.Write("Enter Name Of Directory For Search: ");
            string name = Console.ReadLine();

            string path = "../../../../" + name;

            if (Directory.Exists(path))
            {
                var dir = path;
                Directory.Delete(dir);
                Console.WriteLine("Directory Deleted !");
            }
            else
                Console.WriteLine("Directory Not Exists !");
        }


        public void New()
        {
            //List<User> users = new List<User>();
            //User user1 = new User()
            //{
            //    FirstName = Console.ReadLine(),
            //    LastName = Console.ReadLine(),
            //    Orders = new List<Order>()
            //    {
            //        new Order(){ OrderName= Console.ReadLine(), Quantity= 1 },
            //        new Order(){ OrderName= Console.ReadLine(), Quantity= 2 }
            //    }
            //};

            //User user2 = new User()
            //{
            //    FirstName = "Amir",
            //    LastName = "Amini",
            //    Orders = new List<Order>()
            //    {
            //        new Order(){ OrderName= "Chicken", Quantity=9 },
            //        new Order(){ OrderName= "Salad", Quantity=1 }
            //    }
            //};

            //User user3 = new User()
            //{
            //    FirstName = "Sara",
            //    LastName = "Izadi",
            //    Orders = new List<Order>()
            //    {
            //        new Order(){ OrderName= "Soup", Quantity= 3 },
            //        new Order(){ OrderName= "Salad", Quantity= 1 }
            //    }
            //};

            //users.Add(user1);
            //users.Add(user2);
            //users.Add(user3);



            Console.Write("Enter Name Of File For Search: ");
            string name = Console.ReadLine();
            string path = "../../../../" + name + ".txt".ToLower();

            var query = from u in path select u;

            foreach (var q in query)
            {
                string content = File.ReadAllText(name);  // Read the contents of files
                Console.Write("{0}\n", content); // Insert and display files
                Console.Write("{0}\n", q.ToString());





            }
        }
    }
}
