﻿using FileManager.Common;
using FileManager.Data;
using System;
using System.IO;

namespace FileManager.App
{
    public class ShowInfo
    {
        //public void ShowAllInfos()
        //{
        //    AddOrDeleteData add = new AddOrDeleteData();
        //    add.GetInfo();
        //    Console.WriteLine("==========================");
        //    Console.WriteLine("<your Information>");
        //    Console.WriteLine("your Name: {0}", add.FirstName + " " + add.LastName);
        //    Console.WriteLine("you are {0} years old", add.Age);
        //    Console.WriteLine("your national code: {0}", add.CodeMeli);
        //    Console.WriteLine("you are {0}", add.Gender);
        //    Console.WriteLine("you are {0}", add.Marital);
        //    Console.WriteLine("your email: {0}", add.Email);
        //    Console.WriteLine("your mobile: {0}", add.Mobile);
        //    Console.WriteLine("your address: {0}", add.Address);
        //    Console.WriteLine("your salary: {0}", add.Salary);
        //    Console.WriteLine("description: {0}", add.Description);
        //}

        // This method filters the user name by displaying information about him in the file

        public void ShowInfoOneUser()
        {
            Validation IsValide = new Validation();
            
            IsValide.GetPath();
            if (IsValide.CharValid)
            {
                if (File.Exists(IsValide.ValidName))  // Check for file existence
                {
                    string content = File.ReadAllText(IsValide.ValidName);  // Read the contents of files
                    Console.Write("{0}\n", content); // Insert and display files

                    var files = Directory.GetFiles("../../../../" + IsValide.PathName);
                    foreach (var file in files)
                    {
                        FileInfo info = new FileInfo(file.ToLower());
                        if (info.Name.StartsWith(IsValide.Name.ToString()))  // Check the start letter matching of the input word with the file name
                            Console.WriteLine(info.Name);
                    }
                }
                else
                    Console.WriteLine("File Not Exists !");
            }
            else
                Console.WriteLine("File Access Denied !");
        }

        public void ShowNameFiles()
        {
            Console.Write("Enter Path Name Of File For Search: ");
            string path = Console.ReadLine();

            var files = Directory.GetFiles("../../../../" + path);
            foreach (var file in files)
            {
                FileInfo info = new FileInfo(file);
                Console.WriteLine(info.Name);
            }
        }
    }
}
