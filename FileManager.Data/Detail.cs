﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileManager.Data
{
    public class Detail
    {
        public GenderType Gender { get; set; }
        public MaritalType Marital { get; set; }
        public long CodeMeli { get; set; }
        public long Salary { get; set; }
        public string Description { get; set; }
    }

    public enum GenderType
    {
        Male,
        Female
    }

    public enum MaritalType
    {
        Single,
        Married
    }
}
