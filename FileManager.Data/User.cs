﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileManager.Data
{
    public class User : Detail
    {
        public int UserId = 1;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }    
        public int Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }

        public List<Order> Orders { get; set; }
        public virtual ICollection<Login> Login { get; set; }
    }
}
