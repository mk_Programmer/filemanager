﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileManager.Data
{
    public class Login
    {
        public int LoginID { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public virtual string Role { get; set; }
        public virtual string AccessLimit { get; set; }
        public virtual string State { get; set; }
    }
}
