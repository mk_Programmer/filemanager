﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileManager.Data
{
    public class Order
    {
        public string OrderName { get; set; }
        public int Quantity { get; set; }
    }
}
